"use strict";
/*
1. Екранування символу за допомогою зворотного "слеша" потрібне для того, щоб символ читався як частина тексту, а не як частина коду.
2. Є три способи оголошення функції:
Function declaration – функція оголошується за допомогою ключового слова function.
Function expression - функція записується в змінну, також оголошується за допомогою ключового слова function, вона не має імені.
Named Function expression – функція записується в змінну та має своє ім'я.
3. Hoisting — це коли змінні та оголошення функцій пересуваються вгору своєї області видимості перед виконанням коду незалежно від того, де були оголошені функція або змінна.
*/
let firstName;
let lastName;
let birthday;
do {
    firstName = prompt("Please, enter your first name", "")
    }
while (!firstName || firstName.trim().length === 0);
do {
    lastName = prompt("Please, enter your last name", "");
}
while (!lastName || lastName.trim().length === 0);
birthday = prompt("Please, enter your birthday: dd.mm.yyyy", "01.01.1970");
const transformDate = (enteredDate) => {
    return `${enteredDate.slice(6)}.${enteredDate.slice(3, 5)}.${enteredDate.slice(0, 2)}`;
}
const createNewUser = () => {
return {
    firstName,
    lastName,
    birthday,
    getAge(){
        let today = new Date();
        return Math.floor((today - Date.parse(transformDate(this.birthday)))/(365.25 * 24 * 60 * 60 * 1000));
    },
    getPassword(){
        return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6);
    },
}
}
const newUser = createNewUser();
console.log(newUser);
console.log("------------");
console.log(`${newUser.firstName} ${newUser.lastName} is ${newUser.getAge()} years old.`);
console.log("------------");
console.log(newUser.getPassword());